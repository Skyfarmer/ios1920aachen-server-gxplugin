extern crate glib;
extern crate gstreamer as gst;
extern crate gstreamer_rtsp_server as gst_rtsp;

use gst_rtsp::prelude::*;

fn create_camera_factory() -> gst_rtsp::RTSPMediaFactory {
    let factory = gst_rtsp::RTSPMediaFactory::new();
    factory.set_shared(true);
    factory.set_launch(
        "\
         galaxysrc ! \
         queue ! \
         bayer2rgb ! \
         videoscale ! video/x-raw,width=1280,height=1024 ! \
         videoconvert ! \
         nvh265enc ! \
         rtph265pay name=pay0",
    );
    factory.set_stop_on_disconnect(false);
    factory
}

fn main() {
    gst::init().unwrap();
    let main_loop = glib::MainLoop::new(None, false);

    let server = gst_rtsp::RTSPServer::new();
    let mounts = server.get_mount_points().unwrap();

    let factory = create_camera_factory();
    mounts.add_factory("/camera0", &factory);

    let id = server.attach(None);

    println!(
        "Ready for streaming at rtsp://localhost:{}/{}",
        server.get_bound_port(),
        "camera0"
    );

    main_loop.run();

    glib::source_remove(id);
}
