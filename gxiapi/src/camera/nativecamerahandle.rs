extern crate libc;

use std::os::raw::{c_char, c_uchar};

use crate::error::GxError;
use crate::gx_status_check;

/// A wrapper to the underlying GxiAPI.
///
/// # Safety
/// Note, that this struct does not enforce any correct semantics!
/// Its only safety guarantees include correct memory management as well as handling of any
/// underlying C-Errors. While passing invalid data as an argument should usually be handled
/// gracefully by the underlying API, no guarantees can be made due to its proprietary nature.
/// In addition, this struct assumes its the only owner of the native GX_DEV_HANDLE. Any access to
/// it outside of this struct is undefined behaviour.
///
/// Should you still need to use the following functions directly, refer to the Galaxy SDK
/// manual to make sure to only pass valid arguments.
/// As the vendor only provides the C-Library and the Rust-API is generated dynamically at the runtime,
/// the parameter have a slightly different naming convention.
/// The Rust-API uses a combination of {enum_name}_{enum_value}.
/// Refer to the gxiapi_sys crate for a list of all possible values.
///
/// # Example - C
///
/// enum GX_FEATURE_ID {
///     GX_STRING_DEVICE_VERSION
/// };
///
/// # Example - Rust
///
/// gxiapi_sys::GX_FEATURE_ID_GX_STRING_DEVICE_VERSION
///
/// # Thread Safety
/// The underlying C-API does not state any thread safety and any calls from multiple threads
/// should thus be avoided.
///
/// # See also
/// Camera
pub struct NativeCameraHandle {
    handle: gxiapi_sys::GX_DEV_HANDLE,
    //TODO Return error in all functions when streaming
    is_streaming: bool,
    buffer: Vec<gxiapi_sys::PGX_FRAME_BUFFER>,
}

impl NativeCameraHandle {
    /// Constructs a new NativeCameraHandle using a native GX_DEV_HANDLE.
    /// # Arguments
    /// * handle - A native handle GX_DEV_HANDLE.
    pub fn new(handle: gxiapi_sys::GX_DEV_HANDLE) -> NativeCameraHandle {
        NativeCameraHandle {
            handle,
            is_streaming: false,
            buffer: Vec::new(),
        }
    }

    /// Get a string value from the camera handle.
    /// # Arguments
    /// * value - The identifier of the value to be requested.
    ///
    /// # Safety
    /// See struct description.
    pub unsafe fn get_string_value(&self, value: u32) -> Result<String, GxError> {
        let mut size: u64 = 0;
        //Get length of the message string
        gx_status_check!(gxiapi_sys::GXGetStringLength(
            self.handle,
            value as i32,
            &mut size
        ));

        //Retrieve actual string
        let mut message: Vec<c_char> = Vec::with_capacity(size as usize);
        gx_status_check!(gxiapi_sys::GXGetString(
            self.handle,
            value as i32,
            message.as_mut_ptr(),
            &mut size
        ));
        //Length of vector must be set manually
        message.set_len(size as usize);

        //Convert to Rust-String
        Ok(crate::utilities::from_cstring_to_string(message))
    }

    /// Check whether the camera supports a certain feature.
    /// # Arguments
    /// * feature - The identifier of the feature to be checked.
    ///
    /// # Safety
    /// See struct description.
    pub unsafe fn check_feature(&self, feature: u32) -> Result<bool, GxError> {
        let mut flag = false;
        gx_status_check!(gxiapi_sys::GXIsImplemented(
            self.handle,
            feature as i32,
            &mut flag
        ));
        Ok(flag)
    }

    /// Retrieve an enum value from the camera handle.
    /// # Arguments
    /// * value - The identifier of the value to be requested.
    ///
    /// # Safety
    /// See struct description.
    pub unsafe fn get_enum_value(&self, enum_value: u32) -> Result<i64, GxError> {
        let mut result = 0i64;
        gx_status_check!(gxiapi_sys::GXGetEnum(
            self.handle,
            enum_value as i32,
            &mut result
        ));
        Ok(result)
    }

    /// Retrieve an int value from the camera handle.
    /// # Arguments
    /// * value - The identifier of the value to be requested.
    ///
    /// # Safety
    /// See struct description.
    pub unsafe fn get_int_value(&self, int_value: u32) -> Result<i64, GxError> {
        let mut result = 0i64;
        gx_status_check!(gxiapi_sys::GXGetInt(
            self.handle,
            int_value as i32,
            &mut result
        ));
        Ok(result)
    }

    /// Retrieve a float value from the camera handle.
    /// # Arguments
    /// * value - The identifier of the value to be requested.
    ///
    /// # Safety
    /// See struct description.
    pub unsafe fn get_float_value(&self, float_value: u32) -> Result<f64, GxError> {
        let mut result = 0.0;
        gx_status_check!(gxiapi_sys::GXGetFloat(
            self.handle,
            float_value as i32,
            &mut result
        ));
        Ok(result)
    }

    /// Retrieve the current size of the acquisition buffer in between every capture.
    pub fn get_num_buffer(&self) -> Result<u64, GxError> {
        let mut value = 0;
        unsafe {
            gx_status_check!(gxiapi_sys::GXGetAcqusitionBufferNumber(
                self.handle,
                &mut value
            ));
        }
        Ok(value)
    }

    /// Modify an enum value from the camera handle.
    /// # Arguments
    /// * enum_value - The identifier of the value to be updated.
    /// * value - The actual value assigned to the enum_value.
    ///
    /// # Safety
    /// See struct description.
    pub unsafe fn set_enum_value(&mut self, enum_value: u32, value: i64) -> Result<(), GxError> {
        gx_status_check!(gxiapi_sys::GXSetEnum(self.handle, enum_value as i32, value));
        Ok(())
    }

    /// Modify an int value from the camera handle.
    /// # Arguments
    /// * int_value - The identifier of the value to be updated.
    /// * value - The actual value assigned to the int_value.
    ///
    /// # Safety
    /// See struct description.
    pub unsafe fn set_int_value(&mut self, int_value: u32, value: i64) -> Result<(), GxError> {
        gx_status_check!(gxiapi_sys::GXSetInt(self.handle, int_value as i32, value));
        Ok(())
    }

    /// Modify a float value from the camera handle.
    /// # Arguments
    /// * float_value - The identifier of the value to be updated.
    /// * value - The actual value assigned to the float_value.
    ///
    /// # Safety
    /// See struct description.
    pub unsafe fn set_float_value(&mut self, float_value: u32, value: f64) -> Result<(), GxError> {
        gx_status_check!(gxiapi_sys::GXSetFloat(
            self.handle,
            float_value as i32,
            value
        ));
        Ok(())
    }

    /// Set the number of acquisition buffer in between every capture.
    /// Readjusts both the internal buffer as well as the struct's own buffer.
    /// # Arguments
    /// * value - The size of the acquistion buffer. Refers to the number of frames and __NOT__ the size in memory.
    ///
    /// # Safety
    /// See struct description.
    pub unsafe fn set_num_buffer(&mut self, value: u64) -> Result<(), GxError> {
        gx_status_check!(gxiapi_sys::GXSetAcqusitionBufferNumber(self.handle, value));
        self.buffer = Vec::with_capacity(value as usize);
        Ok(())
    }

    /// Start the camera capture.
    /// # Safety
    /// The function should be 99 % safe to call.
    pub fn start_stream(&mut self) -> Result<(), GxError> {
        if !self.is_streaming {
            //Safe as it is only called when streaming is actually turned off
            unsafe {
                gx_status_check!(gxiapi_sys::GXStreamOn(self.handle));
            }
            self.is_streaming = true;
        }
        Ok(())
    }

    /// Stop the camera capture.
    pub fn stop_stream(&mut self) -> Result<(), GxError> {
        if self.is_streaming {
            //Safe as it is only called when streaming is actually turned on
            unsafe {
                gx_status_check!(gxiapi_sys::GXStreamOff(self.handle));
            }
            self.is_streaming = false;
        }
        Ok(())
    }

    /// Acquire the latest frame from the acquisition buffer.
    /// # Arguments
    /// * img_buffer - The buffer to write to. Ensure that the size of the buffer is large enough
    /// to hold a frame. Function will fail otherwise.
    pub fn acquire_frame_to_buffer(&mut self, img_buffer: &mut [u8]) -> Result<(), GxError> {
        unsafe {
            let mut actual_num_frames = 0;
            //Deque entire buffer from C-API
            gx_status_check!(gxiapi_sys::GXDQAllBufs(
                self.handle,
                self.buffer.as_mut_ptr(),
                self.buffer.capacity() as u32,
                &mut actual_num_frames,
                1000
            ));
            //Buffer length must be set manually
            self.buffer.set_len(actual_num_frames as usize);

            let tmp = match self.buffer.last() {
                Some(v) => &**v,
                None => return Err(GxError::from("Buffer empty".to_owned())),
            };
            //Check whether image acquisition was successful
            if tmp.nStatus != gxiapi_sys::GX_FRAME_STATUS_LIST_GX_FRAME_STATUS_SUCCESS {
                //Return buffer to C-API
                gx_status_check!(gxiapi_sys::GXQAllBufs(self.handle));
                return Err(GxError::from(format!(
                    "Abnormal acquisition: Error code {}",
                    tmp.nStatus
                )));
            }

            //Make sure buffer is large enough
            if img_buffer.len() < tmp.nImgSize as usize {
                //return buffer to C-API
                gx_status_check!(gxiapi_sys::GXQAllBufs(self.handle));
                return Err(GxError::from(format!(
                    "Target buffer too small ({}), at least {} bytes required",
                    img_buffer.len(),
                    tmp.nImgSize
                )));
            }

            //Copy frame from raw buffer to Rust-structure
            std::ptr::copy_nonoverlapping(
                tmp.pImgBuf as *mut c_uchar,
                img_buffer.as_mut_ptr(),
                tmp.nImgSize as usize,
            );

            //Return buffer to C-API
            gx_status_check!(gxiapi_sys::GXQAllBufs(self.handle));
            Ok(())
        }
    }
}

impl Drop for NativeCameraHandle {
    fn drop(&mut self) {
        self.stop_stream().unwrap();
        unsafe {
            gxiapi_sys::GXCloseDevice(self.handle);
        }
    }
}

//Struct is safe to be sent across multiple threads.
unsafe impl Send for NativeCameraHandle {}

unsafe impl Sync for NativeCameraHandle {}
