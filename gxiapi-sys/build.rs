extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rustc-link-lib=gxiapi");
    println!("cargo:rerun-if-changed=wrapper.hpp");
    println!("cargo:rerun-if-changed=GxIAPI.h");

    let bindings = bindgen::Builder::default()
        //Using *.hpp instead of *.h forces bindgen to invoke
        //clang in C++ mode, as the GxIAPI.h provided in the Galaxy
        //SDK is not written in valid C-code.
        .header("wrapper.hpp")
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
