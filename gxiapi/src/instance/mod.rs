use crate::error::GxError;
use crate::gx_status_check;

use std::sync::Arc;
use std::sync::Mutex;

/// Wrapper around the native GX_OPEN_PARAM struct.
mod gxopenparams;

use crate::camera::{features, Camera};
use gxopenparams::GxOpenParams;
use std::collections::HashMap;

/// Sane default, should work with most cameras.
const DEFAULT_ACQUISITION: features::Acquisition = features::Acquisition::Continuous;
const DEFAULT_TRIGGER_SOURCE: features::TriggerSource = features::TriggerSource::Software;
const DEFAULT_TRIGGER_MODE: features::TriggerMode = features::TriggerMode::Off;
const DEFAULT_NUM_BUFFER: u64 = 10;
const DEFAULT_FRAME_RATE: f64 = 35.0;

lazy_static! {
    static ref INSTANCE: Arc<Mutex<Instance>> = {
        let mut instance = Instance::new();
        instance.init().unwrap();
        Arc::new(Mutex::new(instance))
    };
}

/// Instance for initialized the GxIAPI as well as querying and opening connected cameras.
/// This struct uses the Singleton pattern and therefore, cannot be instantiated.
///
/// See get_instance().
pub struct Instance {
    initialized: bool,
    cameras: HashMap<u32, Camera>,
}

impl Instance {
    /// Retrieve a reference to the static Instance.
    ///
    /// __Note__: Due to how statics work in Rust, their drop() function is not called on
    /// application exit. However, in order to properly shutdown the underlying C-Library,
    /// it's crucial to call the close() function before leaving your main() function.
    /// # Example
    /// ```rust,no_run
    /// use gxiapi::instance::Instance;
    /// use gxiapi::camera::features;
    ///
    /// //Retrieve instance.
    /// let mut instance = Instance::get_instance().lock().unwrap();
    ///
    /// //Do camera stuff here.
    ///
    /// //Cleanup GxiAPI before program exits.
    /// gxiapi::instance::Instance::get_instance()
    ///     .lock()
    ///     .unwrap()
    ///     .close()
    ///     .unwrap();
    ///
    /// ```
    pub fn get_instance() -> &'static Arc<Mutex<Instance>> {
        &INSTANCE
    }

    /// Create new Instance.
    ///
    /// __NOTE__: This function may only be called by the lazy_static! definition above.
    /// Multiple instances cause undefined behaviour!
    fn new() -> Instance {
        Instance {
            initialized: false,
            cameras: HashMap::new(),
        }
    }

    /// Initialize underlying C-Library.
    fn init(&mut self) -> Result<(), GxError> {
        if !self.initialized {
            unsafe {
                gx_status_check!(gxiapi_sys::GXInitLib());
            }
            self.initialized = true;
        }
        Ok(())
    }

    /// Open a camera by its zero-based index.
    /// If a camera is already open, it will not reopen it, allowing the use from multiple threads.
    ///
    /// # Arguments
    /// * index - Zero-based index. Must be smaller than count_cameras().
    ///
    /// # Example
    /// ```
    /// use gxiapi::instance::Instance;
    ///
    /// let mut instance = Instance::get_instance().lock().unwrap();
    ///
    /// let camera = match instance.count_cameras().unwrap() {
    ///     c if c > 0 => instance.get_camera(0).unwrap(),
    ///     _ => panic!("No camera connected!")
    /// };
    ///
    /// //Do camera stuff here...
    /// ```
    ///
    /// # See also
    /// * count_cameras() - Retrieves amount of connected cameras.
    pub fn get_camera(&mut self, index: u32) -> Result<Camera, GxError> {
        match self.cameras.get(&index) {
            Some(v) => Ok(v.clone()),
            None => {
                let c = self.open_camera(index)?;
                self.cameras.insert(index, c.clone());
                Ok(c)
            }
        }
    }

    pub fn close_camera(&mut self, index: u32) {
        self.cameras.remove(&index).unwrap();
    }

    /// Open a camera by its zero-based index.
    ///
    /// # Arguments
    /// * index - Zero-based index. Must be smaller than count_cameras().
    ///
    /// # Example
    /// ```
    /// use gxiapi::instance::Instance;
    ///
    /// let mut instance = Instance::get_instance().lock().unwrap();
    ///
    /// let camera = match instance.count_cameras().unwrap() {
    ///     c if c > 0 => instance.get_camera(0).unwrap(),
    ///     _ => panic!("No camera connected!")
    /// };
    ///
    /// //Do camera stuff here...
    /// ```
    ///
    /// # See also
    /// * count_cameras() - Retrieves amount of connected cameras.
    fn open_camera(&mut self, index: u32) -> Result<Camera, GxError> {
        let num_devices = self.count_cameras()?;
        match index {
            x if x < num_devices => {
                let mut handle: gxiapi_sys::GX_DEV_HANDLE = std::ptr::null_mut();
                let mut open_params = GxOpenParams::new(x);
                unsafe {
                    gx_status_check!(gxiapi_sys::GXOpenDevice(&mut *open_params, &mut handle));
                }

                //Set defaults for new camera
                let mut camera = Camera::new(handle);
                camera.set_acquisition(DEFAULT_ACQUISITION)?;
                camera.set_trigger_source(DEFAULT_TRIGGER_SOURCE)?;
                camera.set_trigger_mode(DEFAULT_TRIGGER_MODE)?;
                camera.set_num_buffer(DEFAULT_NUM_BUFFER)?;
                camera.set_frame_rate(DEFAULT_FRAME_RATE)?;

                Ok(camera)
            }
            _ => Err(GxError::from(format!(
                "No device with index {} found",
                index
            ))),
        }
    }

    /// Retrieve amount of connected cameras.
    pub fn count_cameras(&mut self) -> Result<u32, GxError> {
        let mut num_devices = 0;
        unsafe {
            gx_status_check!(gxiapi_sys::GXUpdateDeviceList(&mut num_devices, 1000));
        }
        Ok(num_devices)
    }

    /// Close the underlying C-API.
    /// Must be called before program exits. See get_instance().
    pub fn close(&mut self) -> Result<(), GxError> {
        if self.initialized {
            unsafe {
                gx_status_check!(gxiapi_sys::GXCloseLib());
            }
            self.initialized = false;
        }
        Ok(())
    }
}

impl Drop for Instance {
    fn drop(&mut self) {
        self.close().unwrap();
    }
}

//Make sure to run the tests in single-thread mode
#[cfg(test)]
mod test {
    use crate::instance::Instance;

    //NOTE: Test only works with the microscope connected
    #[test]
    fn open_camera() {
        let mut instance = Instance::get_instance().lock().unwrap();
        assert_eq!(instance.count_cameras().unwrap(), 2);
        assert!(instance.get_camera(0).is_ok());
        assert!(instance.get_camera(1).is_ok());

        instance.close_camera(0);
        instance.close_camera(1);
    }

    #[test]
    fn open_invalid_camera() {
        let mut instance = Instance::get_instance().lock().unwrap();
        assert!(instance.open_camera(555).is_err());
    }
}
