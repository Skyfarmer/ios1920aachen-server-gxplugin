extern crate gxiapi_sys;
#[macro_use]
extern crate lazy_static;

/// Camera related operations.
pub mod camera;
/// Wrapper for underlying GxiAPI-Errors.
pub mod error;
/// General initialization of library.
pub mod instance;
#[macro_use]
/// Repeatedly occurring helper functions.
mod utilities;
