# Introduction
gxiapi is a safe wrapper around gxiapi-sys and thus, the C-Version of the GXiAPI.
However, not all features have been exposed by the underlying library yet.
Current feature list includes:
* Adjusting region of interest for capturing.
* Enabling/Disabling/Modifying frame rate control.
* Frame capturing.

Please consult the Rust-docs for how to use this crate.

# Supported Platforms
Linux is currently the only supported platform due to usage of portions of the GXiAPI, 
which are not available on other platforms.