use glib;
use glib::subclass;
use glib::subclass::prelude::*;

use gst;
use gst::prelude::*;
use gst::subclass::prelude::*;
use gst::ClockTime;

use gst_base;
use gst_base::prelude::*;
use gst_base::subclass::prelude::*;

use glib::subclass::Property;
use gxiapi::camera::features;
use gxiapi::camera::features::Image;
use std::sync::Mutex;

const DEFAULT_INDEX: u32 = 0;

#[derive(Debug, Clone, Copy)]
struct Settings {
    index: u32,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            index: DEFAULT_INDEX,
        }
    }
}

struct State {
    camera: Option<gxiapi::camera::Camera>,
    payload_size: usize,
    frame_rate: u64,
    last_frame_num: u64,
}

impl Default for State {
    fn default() -> Self {
        State {
            camera: None,
            payload_size: 0,
            frame_rate: 0,
            last_frame_num: std::u64::MAX,
        }
    }
}

static PROPERTIES: [Property; 1] = [Property("index", |name| {
    glib::ParamSpec::uint(
        name,
        "Index",
        "Index of camera to use",
        0,
        std::u32::MAX,
        DEFAULT_INDEX,
        glib::ParamFlags::READWRITE,
    )
})];

struct GalaxySrc {
    settings: Mutex<Settings>,
    state: Mutex<State>,
    clock_id: Mutex<Option<gst::ClockId>>,
}

lazy_static! {
    static ref CAT: gst::DebugCategory = gst::DebugCategory::new(
        "galaxysrc",
        gst::DebugColorFlags::empty(),
        Some("Galaxy Camera Source")
    );
}

impl ObjectSubclass for GalaxySrc {
    const NAME: &'static str = "GalaxySrc";
    type ParentType = gst_base::BaseSrc;
    type Instance = gst::subclass::ElementInstanceStruct<Self>;
    type Class = subclass::simple::ClassStruct<Self>;

    glib_object_subclass!();

    fn class_init(klass: &mut subclass::simple::ClassStruct<Self>) {
        klass.set_metadata(
            "Galaxy Camera Source",
            "Source/Video",
            "Receive video footage from Galaxy Cameras",
            "Simon Himmelbauer <himmelba@in.tum.de>",
        );

        let caps = gst::Caps::new_simple(
            "video/x-bayer",
            &[
                ("format", &"rggb"),
                ("width", &gst::IntRange::<i32>::new(1, std::i32::MAX)),
                ("height", &gst::IntRange::<i32>::new(1, std::i32::MAX)),
                (
                    "framerate",
                    &gst::FractionRange::new(
                        gst::Fraction::new(0, 1),
                        gst::Fraction::new(std::i32::MAX, 1),
                    ),
                ),
            ],
        );

        let src_pad_template = gst::PadTemplate::new(
            "src",
            gst::PadDirection::Src,
            gst::PadPresence::Always,
            &caps,
        )
        .unwrap();

        klass.add_pad_template(src_pad_template);
        klass.install_properties(&PROPERTIES);
    }

    fn new() -> Self {
        Self {
            settings: Mutex::new(Settings::default()),
            state: Mutex::new(State::default()),
            clock_id: Mutex::new(None),
        }
    }
}

impl ObjectImpl for GalaxySrc {
    glib_object_impl!();

    fn set_property(&self, obj: &glib::Object, id: usize, value: &glib::Value) {
        let prop = &PROPERTIES[id];
        let basesrc = obj.downcast_ref::<gst_base::BaseSrc>().unwrap();

        match *prop {
            subclass::Property("index", ..) => {
                let mut settings = self.settings.lock().unwrap();
                let index = value.get_some().expect("type checked upstream");
                gst_info!(
                    CAT,
                    obj: basesrc,
                    "Setting camera index from {} to {}",
                    settings.index,
                    index
                );
                settings.index = index;
            }
            _ => unimplemented!(),
        }
    }

    fn get_property(&self, _obj: &glib::Object, id: usize) -> Result<glib::Value, ()> {
        let prop = &PROPERTIES[id];

        match *prop {
            subclass::Property("index", ..) => {
                let settings = self.settings.lock().unwrap();
                Ok(settings.index.to_value())
            }
            _ => unimplemented!(),
        }
    }

    fn constructed(&self, obj: &glib::Object) {
        self.parent_constructed(obj);

        let basesrc = obj.downcast_ref::<gst_base::BaseSrc>().unwrap();
        basesrc.set_live(true);
        basesrc.set_format(gst::Format::Time);
        basesrc.set_do_timestamp(true);
    }
}
impl ElementImpl for GalaxySrc {
    fn change_state(
        &self,
        element: &gst::Element,
        transition: gst::StateChange,
    ) -> Result<gst::StateChangeSuccess, gst::StateChangeError> {
        let basesrc = element.downcast_ref::<gst_base::BaseSrc>().unwrap();

        if let gst::StateChange::ReadyToPaused = transition {
            basesrc.set_live(true);
            gst_debug!(CAT, obj: element, "Setting to live");
        }

        self.parent_change_state(element, transition)
    }
}
impl BaseSrcImpl for GalaxySrc {
    //TODO Create camera at construction time
    fn start(&self, element: &gst_base::BaseSrc) -> Result<(), gst::ErrorMessage> {
        let settings = self.settings.lock().unwrap();
        let mut state = self.state.lock().unwrap();
        *state = Default::default();
        let mut camera = match gxiapi::instance::Instance::get_instance()
            .lock()
            .unwrap()
            .get_camera(settings.index)
        {
            Ok(camera) => camera,
            Err(e) => {
                gst_error!(CAT, obj: element, "{}", e);
                return Err(gst::gst_error_msg!(
                    gst::ResourceError::Failed,
                    [&e.to_string()]
                ));
            }
        };

        //TODO Add offscreen support
        state.payload_size = camera.get_image_config(Image::PayloadSize).unwrap() as usize;
        state.frame_rate = camera.get_frame_rate().unwrap() as u64;
        camera.start_stream().unwrap();
        state.camera = Some(camera);

        gst_info!(CAT, obj: element, "Started");

        Ok(())
    }

    fn stop(&self, element: &gst_base::BaseSrc) -> Result<(), gst::ErrorMessage> {
        *self.state.lock().unwrap() = Default::default();
        self.unlock(element)?;

        gst_info!(CAT, obj: element, "Stopped");

        Ok(())
    }

    fn is_seekable(&self, _element: &gst_base::BaseSrc) -> bool {
        false
    }

    fn create(
        &self,
        element: &gst_base::BaseSrc,
        _offset: u64,
        _length: u32,
    ) -> Result<gst::Buffer, gst::FlowError> {
        let mut state = self.state.lock().unwrap();

        let clock = match element.get_clock() {
            Some(c) => c,
            None => {
                gst_error!(CAT, obj: element, "Cannot operate without clock");
                return Err(gst::FlowError::Error);
            }
        };

        let base_time = element.get_base_time();
        let next_capture_ts = clock.get_time() - base_time;
        let mut next_frame_num =
            next_capture_ts.nanoseconds().unwrap() * state.frame_rate / 1_000_000_000;
        //gst_log!(CAT, obj: element, "CURRENT FRAME NUM {}, {}", next_frame_num, next_capture_ts);
        let (next_capture_ts, dur) = if next_frame_num == state.last_frame_num {
            next_frame_num += 1;
            let next_capture_ts =
                ClockTime::from_nseconds((next_frame_num + 1) * 1_000_000_000 / state.frame_rate);
            let mut clock_wait = self.clock_id.lock().unwrap();
            let id = clock
                .new_single_shot_id(next_capture_ts + base_time)
                .unwrap();
            *clock_wait = Some(id.clone());
            drop(clock_wait);

            gst_log!(
                CAT,
                obj: element,
                "Waiting until {}, now {}",
                next_capture_ts + base_time,
                clock.get_time()
            );
            let (res, jitter) = id.wait();
            gst_log!(CAT, obj: element, "Waited res: {:?} jitter {}", res, jitter);
            self.clock_id.lock().unwrap().take();

            if res == Err(gst::ClockError::Unscheduled) {
                //Got woken up by unlock
                gst_debug!(CAT, obj: element, "Flushing");
                return Err(gst::FlowError::Flushing);
            }

            let dur = ClockTime::from_nseconds(1000000000 / state.frame_rate);
            (next_capture_ts, dur)
        } else {
            gst_log!(
                CAT,
                obj: element,
                "No need to wait for next frame time {}, next frame {}, prev {}",
                next_capture_ts,
                next_frame_num,
                state.last_frame_num
            );
            let next_frame_ts =
                ClockTime::from_nseconds((next_frame_num + 1) * 1_000_000_000 / state.frame_rate);
            let dur = next_frame_ts - next_capture_ts;
            (next_capture_ts, dur)
        };
        state.last_frame_num = next_frame_num;

        let mut buffer = gst::Buffer::with_size(state.payload_size).unwrap();
        {
            let buffer = buffer.get_mut().unwrap();

            buffer.set_dts(ClockTime::none());
            buffer.set_pts(next_capture_ts);
            buffer.set_duration(dur);
            let mut buffer = buffer.map_writable().unwrap();
            if let Err(e) = state
                .camera
                .as_mut()
                .unwrap()
                .copy_frame(&mut buffer)
            {
                gst_error!(CAT, obj: element, "Couldn't acquire image: {}", e);
                return Err(gst::FlowError::Error);
            }
        }

        gst_debug!(CAT, obj: element, "Produced buffer {:?}", buffer);

        Ok(buffer)
    }

    fn query(&self, element: &gst_base::BaseSrc, query: &mut gst::QueryRef) -> bool {
        use gst::QueryView;

        match query.view_mut() {
            QueryView::Scheduling(ref mut q) => {
                q.set(gst::SchedulingFlags::SEQUENTIAL, 1, -1, 0);
                q.add_scheduling_modes(&[gst::PadMode::Push]);
                true
            }
            _ => BaseSrcImplExt::parent_query(self, element, query),
        }
    }

    fn set_caps(
        &self,
        element: &gst_base::BaseSrc,
        caps: &gst::Caps,
    ) -> Result<(), gst::LoggableError> {
        let info = match gst_video::VideoInfo::from_caps(caps) {
            Ok(info) => info,
            Err(e) => {
                return Err(gst_loggable_error!(
                    CAT,
                    "Failed to build 'VideoInfo' from caps {}:{}",
                    caps,
                    e
                ))
            }
        };

        gst_debug!(CAT, obj: element, "Configuring for caps {}", caps);

        //TODO Remove once fixed negotiation is implemented
        let mut state = self.state.lock().unwrap();
        let camera = state.camera.as_mut().unwrap();
        camera.stop_stream().unwrap();
        let camera_fps = camera.get_frame_rate().unwrap();
        //Handle framerate
        {
            let requested_fps = {
                let (num, den) = info.fps().into();
                num as f64 / den as f64
            };
            if camera_fps < requested_fps {
                return Err(gst_loggable_error!(
                    CAT,
                    "Requested framerate ({}) is higher than supported ({})",
                    requested_fps,
                    camera_fps
                ));
            }
        }
        //Handle Width/Height
        {
            let (camera_width, camera_height) = (
                camera.get_image_config(Image::Width).unwrap() as u32,
                camera.get_image_config(Image::Height).unwrap() as u32,
            );
            if info.width() != camera_width {
                return Err(gst_loggable_error!(
                    CAT,
                    "Widtħ {} does not match camera source ({})",
                    info.width(),
                    camera_width
                ));
            }
            if info.height() != camera_height {
                return Err(gst_loggable_error!(
                    CAT,
                    "Height {} does not match camera source ({})",
                    info.height(),
                    camera_height
                ));
            }
        }

        camera.start_stream().unwrap();

        Ok(())
    }

    fn fixate(&self, element: &gst_base::BaseSrc, caps: gst::Caps) -> gst::Caps {
        let mut caps = gst::Caps::truncate(caps);
        {
            let state = self.state.lock().unwrap();
            let camera = state.camera.as_ref().unwrap();
            let caps = caps.make_mut();
            let s = caps.get_mut_structure(0).unwrap();
            //Force GStreamer to use our resolution
            s.set_value(
                "width",
                (camera.get_image_config(features::Image::Width).unwrap() as i32).to_send_value(),
            );
            s.set_value(
                "height",
                (camera.get_image_config(features::Image::Height).unwrap() as i32).to_send_value(),
            );
            s.set_value(
                "framerate",
                gst::Fraction::approximate_f64(camera.get_frame_rate().unwrap())
                    .unwrap()
                    .to_send_value(),
            );
        }

        gst_debug!(CAT, obj: element, "Fixate caps: {}", caps);

        self.parent_fixate(element, caps)
    }

    fn unlock(&self, element: &gst_base::BaseSrc) -> Result<(), gst::ErrorMessage> {
        gst_debug!(CAT, obj: element, "Unlocking");
        if let Some(clock_id) = self.clock_id.lock().unwrap().take() {
            clock_id.unschedule();
        }
        Ok(())
    }
}

pub fn register(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    gst::Element::register(
        Some(plugin),
        "galaxysrc",
        gst::Rank::None,
        GalaxySrc::get_type(),
    )
}
