use std::convert::TryFrom;
use std::fmt::Formatter;
use std::os::raw::c_char;
use std::ptr::null_mut;

use crate::gx_status_check;

#[derive(Debug)]
pub struct GxError {
    message: String,
}

impl GxError {
    fn get_gx_error(mut value: i32) -> Option<String> {
        let mut error_size: u64 = 0;
        unsafe {
            gx_status_check!(
                gxiapi_sys::GXGetLastError(&mut value, null_mut(), &mut error_size),
                None
            );
        }

        let mut message: Vec<c_char> = Vec::with_capacity(error_size as usize);
        unsafe {
            gx_status_check!(
                gxiapi_sys::GXGetLastError(&mut value, message.as_mut_ptr(), &mut error_size),
                None
            );
            message.set_len(error_size as usize);
        }

        Some(crate::utilities::from_cstring_to_string(message))
    }
}

impl TryFrom<i32> for GxError {
    type Error = GxError;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        match Self::get_gx_error(value) {
            Some(message) => Ok(GxError::from(message)),
            None => Err(GxError::from(format!("Error code {} is not valid", value))),
        }
    }
}

impl From<String> for GxError {
    fn from(message: String) -> Self {
        GxError { message }
    }
}

impl std::fmt::Display for GxError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for GxError {}

#[cfg(test)]
mod error_tests {
    use std::convert::TryFrom;

    use crate::error::GxError;

    #[test]
    fn get_error() {
        let mut device_num: u32 = 0;
        unsafe {
            assert_eq!(
                gxiapi_sys::GX_STATUS_LIST_GX_STATUS_SUCCESS,
                gxiapi_sys::GXCloseLib()
            );
        }

        let status = unsafe { gxiapi_sys::GXUpdateDeviceList(&mut device_num, 1000) };
        assert_ne!(status, gxiapi_sys::GX_STATUS_LIST_GX_STATUS_SUCCESS);
        let error = GxError::try_from(status);
        assert!(error.is_ok());
        let error = error.unwrap();
        assert_ne!(error.message.len(), 0);

        unsafe {
            assert_eq!(
                gxiapi_sys::GX_STATUS_LIST_GX_STATUS_SUCCESS,
                gxiapi_sys::GXInitLib()
            );
        }
    }
}
