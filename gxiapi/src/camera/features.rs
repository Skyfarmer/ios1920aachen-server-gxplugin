use crate::error::GxError;
use std::convert::TryFrom;

/// Retrieve general information from the hardware of the device.
pub enum Device {
    Vendor,
    Model,
    SerialNumber,
    Version,
}

/// Map enum-values to their native equivalent.
impl Into<u32> for Device {
    fn into(self) -> u32 {
        match self {
            Self::Vendor => gxiapi_sys::GX_FEATURE_ID_GX_STRING_DEVICE_VENDOR_NAME,
            Self::Model => gxiapi_sys::GX_FEATURE_ID_GX_STRING_DEVICE_MODEL_NAME,
            Self::SerialNumber => gxiapi_sys::GX_FEATURE_ID_GX_STRING_DEVICE_SERIAL_NUMBER,
            Self::Version => gxiapi_sys::GX_FEATURE_ID_GX_STRING_DEVICE_VERSION,
        }
    }
}

/// Retrieve capabilities of the camera's sensor.
/// Unlike the Image enum, Sensor only requests values that are constant
/// and thus, only need to be requested once and can be cached for the remaining lifetime of the
/// camera.
pub enum Sensor {
    HeightMax,
    WidthMax,
}

/// Map enum-values to their native equivalent.
impl Into<u32> for Sensor {
    fn into(self) -> u32 {
        match self {
            Self::HeightMax => gxiapi_sys::GX_FEATURE_ID_GX_INT_HEIGHT_MAX,
            Self::WidthMax => gxiapi_sys::GX_FEATURE_ID_GX_INT_WIDTH_MAX,
        }
    }
}

/// Retrieve current settings of the camera's sensor.
/// Unlike the Sensor enum, these values can be dynamically adjusted.
pub enum Image {
    Height,
    Width,
    OffsetX,
    OffsetY,
    PixelFormat,
    PayloadSize,
}

/// Map enum-values to their native equivalent.
impl Into<u32> for Image {
    fn into(self) -> u32 {
        match self {
            Self::Height => gxiapi_sys::GX_FEATURE_ID_GX_INT_HEIGHT,
            Self::Width => gxiapi_sys::GX_FEATURE_ID_GX_INT_WIDTH,
            Self::PixelFormat => gxiapi_sys::GX_FEATURE_ID_GX_ENUM_PIXEL_FORMAT,
            Self::PayloadSize => gxiapi_sys::GX_FEATURE_ID_GX_INT_PAYLOAD_SIZE,
            Self::OffsetX => gxiapi_sys::GX_FEATURE_ID_GX_INT_OFFSET_X,
            Self::OffsetY => gxiapi_sys::GX_FEATURE_ID_GX_INT_OFFSET_Y,
        }
    }
}

/// Acquisition mode. Look at the Galaxy SDK for further information.
pub enum Acquisition {
    Continuous,
}

/// Map enum-values to their native equivalent.
impl Into<i64> for Acquisition {
    fn into(self) -> i64 {
        match self {
            Self::Continuous => gxiapi_sys::GX_ACQUISITION_MODE_ENTRY_GX_ACQ_MODE_CONTINUOUS as i64,
        }
    }
}

/// Try to map native integer values to the enum.
impl TryFrom<i64> for Acquisition {
    type Error = GxError;

    fn try_from(value: i64) -> Result<Self, Self::Error> {
        match value {
            x if x == gxiapi_sys::GX_ACQUISITION_MODE_ENTRY_GX_ACQ_MODE_CONTINUOUS as i64 => {
                Ok(Self::Continuous)
            }
            _ => Err(GxError::from(format!(
                "{} is not a valid acquisition mode",
                value
            ))),
        }
    }
}

/// Trigger mode. Look at the Galaxy SDK for further information.
pub enum TriggerMode {
    Off,
    On,
}

/// Map enum-values to their native equivalent.
impl Into<i64> for TriggerMode {
    fn into(self) -> i64 {
        match self {
            Self::Off => gxiapi_sys::GX_TRIGGER_MODE_ENTRY_GX_TRIGGER_MODE_OFF as i64,
            Self::On => gxiapi_sys::GX_TRIGGER_MODE_ENTRY_GX_TRIGGER_MODE_ON as i64,
        }
    }
}

/// Try to map native integer values to the enum.
impl TryFrom<i64> for TriggerMode {
    type Error = GxError;

    fn try_from(value: i64) -> Result<Self, Self::Error> {
        match value {
            x if x == gxiapi_sys::GX_TRIGGER_MODE_ENTRY_GX_TRIGGER_MODE_OFF as i64 => Ok(Self::Off),
            x if x == gxiapi_sys::GX_TRIGGER_MODE_ENTRY_GX_TRIGGER_MODE_ON as i64 => Ok(Self::On),
            _ => Err(GxError::from(format!(
                "{} is not a valid trigger mode",
                value
            ))),
        }
    }
}

/// Trigger source. Look at the Galaxy SDK for further information.
pub enum TriggerSource {
    Software,
}

/// Map enum-values to their native equivalent.
impl Into<i64> for TriggerSource {
    fn into(self) -> i64 {
        match self {
            Self::Software => gxiapi_sys::GX_TRIGGER_SOURCE_ENTRY_GX_TRIGGER_SOURCE_SOFTWARE as i64,
        }
    }
}

/// Try to map native integer values to the enum.
impl TryFrom<i64> for TriggerSource {
    type Error = GxError;

    fn try_from(value: i64) -> Result<Self, Self::Error> {
        match value {
            x if x == gxiapi_sys::GX_TRIGGER_SOURCE_ENTRY_GX_TRIGGER_SOURCE_SOFTWARE as i64 => {
                Ok(Self::Software)
            }
            _ => Err(GxError::from(format!(
                "{} is not a valid trigger source",
                value
            ))),
        }
    }
}

//TODO Add PixelFormat Enum
