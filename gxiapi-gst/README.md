# Introduction
gxiapi-gst is a GStreamer source plugin.

# Installation
Make sure to install gstreamer, gstreamer-base, gstreamer-video as well as their development files on your system.
See the [official gstreamer repository for more.](https://gitlab.freedesktop.org/gstreamer/gstreamer-rs#gstreamer-rs)

Building this crate will result in a platform specific gxiapigst(.so/.dll) in the target folder. Copy this file
to a location where it can be found by GStreamer. Otherwise, set your GST_PLUGIN_PATH appropriately.

# Examples
This project ships with two examples. They can be run with cargo run --example {name}, where name is one
of the examples below. Make sure to properly install the library as mentioned above.
* display - Displays a live feed from the first camera found on the system.
* rtsp - Launch an RTSP-live stream accessible with any RTSP-capable video player. _NOTE_: Due to the high
resolution produced by the cameras, the stream is HEVC-encoded using NVENC. In case you do not own an Nvidia
graphics card or do not have the nvenc plugin installed, adjust the pipeline accordingly.

# GStreamer pipelines to play around

### Display on screen
`gst-launch-1.0 -e galaxysrc ! bayer2rgb ! queue ! videoconvert ! autovideosink`
### Record H.264-encoded videostream into a QuickTime-container
`gst-launch-1.0 -e galaxysrc index=0 ! bayer2rgb ! queue ! videoconvert ! x264enc ! qtmux ! filesink location=./test.mp4`