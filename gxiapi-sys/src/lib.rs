#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(clippy::unreadable_literal)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn open_library() {
        unsafe {
            let status = GXInitLib();

            assert_eq!(status, GX_STATUS_LIST_GX_STATUS_SUCCESS);

            let _status = GXCloseLib();
        }
    }
}
