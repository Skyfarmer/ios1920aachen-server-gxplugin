# Introduction
gxiapi-sys is a wrapper around the C Galaxy Camera SDK.
Bindings are generated dynamically at compile time from the wrapper.hpp
and GxIAPI.h header files. Finally, the crate is linked against libgxiapi.so/gxiapi.dll,
which must be installed in one of the system locations where the linker can find it.

# Supported platforms
All supported platforms by the GxIAPI should be compatible.