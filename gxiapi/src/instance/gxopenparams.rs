use std::ffi::CString;
use std::ops::{Deref, DerefMut};

/// Wrapper to the native GX_OPEN_PARAM struct.
pub struct GxOpenParams {
    inner: gxiapi_sys::GX_OPEN_PARAM,
}

impl GxOpenParams {
    /// Construct new instance.
    /// Cameras are always opened in exclusive mode.
    /// # Arguments
    /// * index - Zero-based index of the camera to be opened.
    /// # See also
    /// Instance to retrieve the amount of connected cameras.
    pub fn new(index: u32) -> GxOpenParams {
        // The index needs to be passed as a C-String for some reason
        // Since we allocate memory for it, we are also
        // responsible for deallocating it properly later.

        // Note that the index is increased by one as the underlying
        // C-API uses one-based indices. However, the author of this library
        // finds this a very distasteful design choice and handles this
        // dirty work in order to provide the caller of this function with a proper
        // zero-based index.
        let index = CString::new(format!("{}", index + 1)).unwrap().into_raw();
        GxOpenParams {
            inner: gxiapi_sys::GX_OPEN_PARAM {
                accessMode: gxiapi_sys::GX_ACCESS_MODE_GX_ACCESS_EXCLUSIVE as i32,
                openMode: gxiapi_sys::GX_OPEN_MODE_GX_OPEN_INDEX as i32,
                pszContent: index,
            },
        }
    }
}

impl Drop for GxOpenParams {
    fn drop(&mut self) {
        unsafe {
            // Deallocate C-String allocated in new()
            // Safety: pszContent must be valid due to its only way of constructing is in
            // the new() function
            CString::from_raw(self.inner.pszContent)
        };
    }
}

/// Deref to native GX_OPEN_PARAM struct
impl DerefMut for GxOpenParams {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

impl Deref for GxOpenParams {
    type Target = gxiapi_sys::GX_OPEN_PARAM;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}
