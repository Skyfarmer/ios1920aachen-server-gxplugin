use std::ffi::CStr;

/// Convenience macro to check whether call of native C-GXIAPI function was successful or not.
///
/// There are three variants provided:
/// # ( $:expr)
/// Return a Err(GxError) automatically.
/// ## Arguments
/// * x - Call to native function.
///
/// # ( $x:expr, $r:expr)
/// If native function fails, use custom expression, ignoring exact error code.
/// Useful for cleanups or arbitrary return types.
/// ## Arguments
/// * x - Call to native function.
/// * r - Expression to be executed in case of failure.
///
/// # ( $x:expr, final => $r:tt)
/// If native function fails, use provided custom function/closure
/// with status code as a parameter.
/// Useful for cleanups or arbitrary return types.
/// ## Arguments
/// * x - Call to native function.
/// * r - Function/closure to be executed in case of failure.
#[macro_export]
macro_rules! gx_status_check {
    ( $x:expr ) => {
    {
        use std::convert::TryFrom;
        let callback = |status| Err(GxError::try_from(status).unwrap());
        gx_status_check!($x, final => callback);
    }
    };
    ( $x:expr, $r:expr) => {
        let callback = |_| $r;
        gx_status_check!($x, final => callback);
    };
    ( $x:expr, final => $r:tt) => {
        let status = $x;
        if status != gxiapi_sys::GX_STATUS_LIST_GX_STATUS_SUCCESS {
            return $r(status);
        }
    };
}

/// Convenience function to convert a buffer containing a raw C-string into a Rust string.
/// If the buffer contains invalid data, an empty string will be returned.
pub fn from_cstring_to_string(input: Vec<i8>) -> String {
    CStr::from_bytes_with_nul(&input.iter().map(|x| *x as u8).collect::<Vec<u8>>())
        .unwrap_or_default()
        .to_string_lossy()
        .into_owned()
}
