# Introduction
The GXPlugin allows the usage of Daheng Imaging cameras in a GStreamer pipeline. Currently only supports Linux.

## Structure
* gxiapi-sys - Rust bindings to the native GXIAPI C-library. Bindings are generated dynamically at compile time from the appropriate header.
* gxiapi - A safe Rust wrapper building on top of gxiapi-sys. Can be used independently from GStreamer (e.g. in a windowing toolkit, FFMPEG, etc.).
* gxiapi-gst - GStreamer Plugin
