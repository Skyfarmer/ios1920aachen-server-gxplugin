/// Collection of enums to request/set certain features/values.
pub mod features;
/// A wrapper to the underlying GxiAPI.
mod nativecamerahandle;

use crate::error::GxError;
use std::convert::TryInto;
use std::sync::{Arc, Mutex};
use std::time::Duration;

struct Frame {
    frame_rate: f64,
    timestamp: std::time::Instant,
    data: Vec<u8>,
}

/// A handle to an open camera. It can be cloned to allow access from multiple threads.
///
/// All operations applicable are based on the C-Library of the GxIAPI. Please refer to the
/// manufacturer's manual for how the different acquisition/trigger modes, etc. on affect
/// capturing.
#[derive(Clone)]
pub struct Camera {
    camera: Arc<Mutex<nativecamerahandle::NativeCameraHandle>>,
    last_frame: Arc<Mutex<Option<Frame>>>,
}

impl Camera {
    /// Construct a new Camera instance using a native GX_DEV_HANDLE.
    /// This struct assumes to be only owner of the GX_DEV_HANDLE.
    /// Access to the handle from outside of this scope leads to undefined behaviour.
    ///
    /// This function cannot be called by the user of this library directly. Instead,
    /// use the CameraBuilder instead.
    /// # Arguments
    /// * handle - A native GX_DEV_HANDLE.
    /// # See also
    /// * CameraBuilder
    pub fn new(handle: gxiapi_sys::GX_DEV_HANDLE) -> Self {
        Camera {
            camera: Arc::new(Mutex::new(nativecamerahandle::NativeCameraHandle::new(
                handle,
            ))),
            last_frame: Arc::new(Mutex::new(None)),
        }
    }

    /// Get information about the camera hardware.
    /// # Arguments
    ///
    /// * 'info' - The information to be requested.
    pub fn get_camera_info(&self, info: features::Device) -> Result<String, GxError> {
        //Call is safe as features::Device only converts into valid Gxi-Parameters
        unsafe { self.camera.lock().unwrap().get_string_value(info.into()) }
    }

    /// Get information about the sensor of the camera.
    /// # Arguments
    ///
    /// * 'info' - The information to be requested.
    pub fn get_sensor_info(&self, info: features::Sensor) -> Result<i64, GxError> {
        //Safe as features::Sensor only converts into valid identifiers
        unsafe { self.camera.lock().unwrap().get_int_value(info.into()) }
    }

    /// Get information regarding image capturing.
    /// # Arguments
    ///
    /// * 'info' - The information to be requested.
    pub fn get_image_config(&self, info: features::Image) -> Result<i64, GxError> {
        use features::Image;
        //Match to distinguish between Int and Enum values
        match info {
            //Match values requested as an int
            Image::Height | Image::Width | Image::OffsetX | Image::OffsetY | Image::PayloadSize => {
                //Call is safe as only int values are converted
                unsafe { Ok(self.camera.lock().unwrap().get_int_value(info.into())?) }
            }
            //Match values requested as an enum
            Image::PixelFormat => unsafe {
                //Call is safe as only enum values are converted
                Ok(self.camera.lock().unwrap().get_enum_value(info.into())?)
            },
        }
    }

    /// Get size of the internal buffer.
    ///
    /// _Note_: This function returns the amount of frames the buffer can store and not the size
    /// of the buffer in memory.
    pub fn get_num_buffer(&self) -> Result<u64, GxError> {
        self.camera.lock().unwrap().get_num_buffer()
    }

    /// Retrieve the current acquisition mode.
    pub fn get_acquistion(&self) -> Result<features::Acquisition, GxError> {
        let e = gxiapi_sys::GX_FEATURE_ID_GX_ENUM_ACQUISITION_MODE;
        //Safe as e is a constant and thus always safe to call
        unsafe { Ok(self.camera.lock().unwrap().get_enum_value(e)?.try_into()?) }
    }

    /// Retrieve the current trigger mode.
    pub fn get_trigger_mode(&self) -> Result<features::TriggerMode, GxError> {
        let e = gxiapi_sys::GX_FEATURE_ID_GX_ENUM_TRIGGER_MODE;
        //Safe as e is constant and thus always valid
        unsafe { Ok(self.camera.lock().unwrap().get_enum_value(e)?.try_into()?) }
    }

    /// Retrieve the current trigger source.
    pub fn get_trigger_source(&self) -> Result<features::TriggerSource, GxError> {
        let e = gxiapi_sys::GX_FEATURE_ID_GX_ENUM_TRIGGER_SOURCE;
        //Safe as e is constant
        unsafe { Ok(self.camera.lock().unwrap().get_enum_value(e)?.try_into()?) }
    }

    /// Retrieve the current limit for frame rate control.
    pub fn get_frame_rate(&self) -> Result<f64, GxError> {
        //Safe as function is called with a constant
        unsafe {
            self.camera
                .lock()
                .unwrap()
                .get_float_value(gxiapi_sys::GX_FEATURE_ID_GX_FLOAT_ACQUISITION_FRAME_RATE)
        }
    }

    /// Set a configuration item regarding image capturing.
    /// # Arguments
    ///
    /// * 'info' - The configuration to be set.
    /// * 'value' - The value to be set.
    pub fn set_image_config(&mut self, info: features::Image, value: i64) -> Result<(), GxError> {
        use features::Image;
        match info {
            Image::Height | Image::Width => {
                if self.check_constraints(&info, value)? {
                    //Call is safe for both info and value
                    //info: Image::Height/Width convert into valid values
                    //value: check_constraints() ensures that the maximum height/width of the camera
                    //sensor are not exceeded
                    unsafe {
                        self.camera
                            .lock()
                            .unwrap()
                            .set_int_value(info.into(), value)?
                    };
                } else {
                    return Err(GxError::from(format!(
                        "Value {} exceeds supported maximum height/width of camera",
                        value
                    )));
                }
            }
            _ => unimplemented!(),
        }
        Ok(())
    }

    /// Set the size of the internal buffer.
    ///
    /// # Arguments
    /// * num_buffer - The amount of frames the buffer can hold.
    pub fn set_num_buffer(&mut self, num_buffer: u64) -> Result<(), GxError> {
        //The official code samples from the camera vendor check in between 5-450.
        //This is not documented in their manual, however, it seems sane to follow their
        //convention.
        if num_buffer >= 5 && num_buffer <= 450 {
            unsafe { self.camera.lock().unwrap().set_num_buffer(num_buffer) }
        } else {
            Err(GxError::from(format!(
                "Requested buffer size ({}) is not within constraints (5-450)",
                num_buffer
            )))
        }
    }

    /// Check whether a certain value is valid for a given property.
    ///
    /// # Arguments
    /// * property - Related property.
    /// * value - Value to check for the provided property.
    fn check_constraints(&self, property: &features::Image, value: i64) -> Result<bool, GxError> {
        use features::Image;
        let max = match property {
            Image::Height => self.get_sensor_info(features::Sensor::HeightMax)?,
            Image::Width => self.get_sensor_info(features::Sensor::WidthMax)?,
            _ => unimplemented!(),
        };

        Ok(value > 0 && value <= max)
    }

    /// Set the acquisition mode.
    /// # Arguments
    /// * mode - Desired acquisition mode
    /// # See also
    /// * features::Acquisition
    pub fn set_acquisition(&mut self, mode: features::Acquisition) -> Result<(), GxError> {
        use features::Acquisition;
        let e = gxiapi_sys::GX_FEATURE_ID_GX_ENUM_ACQUISITION_MODE;
        match mode {
            Acquisition::Continuous => unsafe {
                //Safe as e is a constant and mode always converts into a valid value
                self.camera.lock().unwrap().set_enum_value(e, mode.into())
            },
        }
    }

    /// Set the trigger mode.
    /// # Arguments
    /// * mode - Desired trigger mode
    /// # See also
    /// * features::TriggerMode
    pub fn set_trigger_mode(&mut self, mode: features::TriggerMode) -> Result<(), GxError> {
        use features::TriggerMode;
        let e = gxiapi_sys::GX_FEATURE_ID_GX_ENUM_TRIGGER_MODE;
        match mode {
            TriggerMode::Off | TriggerMode::On => unsafe {
                //Safe as e is a constant and mode always converts into fixed valid values
                self.camera.lock().unwrap().set_enum_value(e, mode.into())
            },
        }
    }

    /// Set the trigger source.
    /// # Arguments
    /// * source - Desired source for a trigger.
    /// # See also
    /// * features::TriggerSource
    pub fn set_trigger_source(&mut self, source: features::TriggerSource) -> Result<(), GxError> {
        use features::TriggerSource;
        let e = gxiapi_sys::GX_FEATURE_ID_GX_ENUM_TRIGGER_SOURCE;
        match source {
            TriggerSource::Software => unsafe {
                //Safe as e is constant and source always translates into fixed valid values
                self.camera.lock().unwrap().set_enum_value(e, source.into())
            },
        }
    }

    /// Set the limit for frame rate control.
    /// # Arguments
    /// * fps - Limit for frame rate control.
    pub fn set_frame_rate(&mut self, fps: f64) -> Result<(), GxError> {
        //TODO Activate frame rate control first, before setting a value
        unsafe {
            if self
                .camera
                .lock()
                .unwrap()
                .check_feature(gxiapi_sys::GX_FEATURE_ID_GX_ENUM_ACQUISITION_FRAME_RATE_MODE)?
            {
                //FIXME No constraints are checked
                self.camera.lock().unwrap().set_float_value(
                    gxiapi_sys::GX_FEATURE_ID_GX_FLOAT_ACQUISITION_FRAME_RATE,
                    fps,
                )
            } else {
                Err(GxError::from(
                    "Frame rate control not supported by camera".to_owned(),
                ))
            }
        }
    }

    /// Initiate camera acquisition.
    /// This function needs to be called before any frame is acquired.
    ///
    /// _Note_: While streaming is on, many settings cannot be modified. Deactivate streaming
    /// before applying any changes.
    pub fn start_stream(&mut self) -> Result<(), GxError> {
        self.camera.lock().unwrap().start_stream()
    }

    /// Stops camera acquisiton.
    pub fn stop_stream(&mut self) -> Result<(), GxError> {
        self.camera.lock().unwrap().stop_stream()
    }

    /// Acquire a frame into a provided buffer.
    ///
    /// # Arguments
    /// * 'buffer' - A buffer to write the frame into. It's important that the buffer is larger than
    /// the size of the resulting frame. See the example below for how to achieve that.
    ///
    /// # Safety
    /// This functions checks whether the buffer can hold the entire frame data.
    ///
    /// # Example
    /// ```rust,no_run
    /// use gxiapi::instance::Instance;
    /// use gxiapi::camera::features;
    ///
    /// let mut instance = Instance::get_instance().lock().unwrap();
    /// let mut camera = instance.get_camera(0).unwrap();
    ///
    /// let payload_size = camera.get_image_config(features::Image::PayloadSize).unwrap() as usize;
    /// let mut buffer = vec![0;payload_size];
    /// camera.copy_frame(&mut buffer).unwrap();
    /// ```
    pub fn copy_frame(&mut self, buffer: &mut [u8]) -> Result<(), GxError> {
        //TODO Use shallow copys
        let mut last_frame = self.last_frame.lock().unwrap();
        match last_frame.as_ref() {
            Some(frame) => {
                let frame_duration = Duration::from_secs_f64(1.0 / frame.frame_rate);
                if let Some(_) = std::time::Instant::now().checked_duration_since(frame.timestamp + frame_duration) {
                    //Not in time reacquire
                    let mut buffer = vec![0; frame.data.len()];
                    self.camera
                        .lock()
                        .unwrap()
                        .acquire_frame_to_buffer(&mut buffer)?;
                    let frame = Frame {
                        frame_rate: frame.frame_rate,
                        data: buffer,
                        timestamp: std::time::Instant::now(),
                    };
                    *last_frame = Some(frame)
                }
            }
            None => {
                //TODO Refactor into one function
                let mut buffer =
                    vec![0; self.get_image_config(features::Image::PayloadSize)? as usize];
                self.camera
                    .lock()
                    .unwrap()
                    .acquire_frame_to_buffer(&mut buffer)?;
                let frame = Frame {
                    frame_rate: self.get_frame_rate()?,
                    data: buffer,
                    timestamp: std::time::Instant::now(),
                };
                *last_frame = Some(frame)
            }
        };

        buffer.copy_from_slice(&last_frame.as_ref().unwrap().data);

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::camera::features::{Device, Image};
    use crate::instance::Instance;

    //NOTE: This test only works if a camera is connected
    #[test]
    fn get_information() {
        let mut instance = Instance::get_instance().lock().unwrap();
        let camera = instance.get_camera(0).unwrap();
        eprintln!("{}", camera.get_camera_info(Device::Vendor).unwrap());
        eprintln!("{}", camera.get_camera_info(Device::Model).unwrap());
        eprintln!("{}", camera.get_camera_info(Device::SerialNumber).unwrap());
        eprintln!("{}", camera.get_camera_info(Device::Version).unwrap());
    }

    #[test]
    fn acquire_frame_information() {
        let mut instance = Instance::get_instance().lock().unwrap();
        let camera = instance.get_camera(0).unwrap();

        let width = camera.get_image_config(Image::Width).unwrap() as i32;
        let height = camera.get_image_config(Image::Height).unwrap() as i32;
        let pixel_format = camera.get_image_config(Image::PixelFormat).unwrap() as u32;

        assert_eq!(2560, width);
        assert_eq!(2048, height);
        assert_eq!(
            gxiapi_sys::GX_PIXEL_FORMAT_ENTRY_GX_PIXEL_FORMAT_BAYER_RG8,
            pixel_format
        );
    }

    #[test]
    fn acquire_image() {
        let mut instance = Instance::get_instance().lock().unwrap();
        let mut camera = instance.get_camera(0).unwrap();
        let payload_length = camera.get_image_config(Image::PayloadSize).unwrap() as usize;

        let mut buffer = vec![0; payload_length];

        camera.start_stream().unwrap();
        camera.copy_frame(&mut buffer).unwrap();
        camera.stop_stream().unwrap();

        assert_eq!(buffer.len(), payload_length);
    }
}
